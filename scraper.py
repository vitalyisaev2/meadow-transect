#!/usr/bin/python3.2

from meadow_transect import sites
from optparse import OptionParser

def parse_args():
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option("-i", "--infile", dest="infile", type = "string", help="Path to *.csv file with a list of sites")
    parser.add_option("-o", "--outshp", dest="outshp", type = "string", help="Path to *.shp storing the output with geospatial data")
    parser.add_option("-a", "--amount", dest="amount", type = "int", help="Amount of transects per every site")
    parser.add_option("-p", "--precision", dest="precision", type = "int",help="Amount of points per every transect")
    parser.add_option("-e", "--extent_multiplication", dest="extent_multiplication", type = "int",help="Multiplier value for the extent set individually for every site")
    (options, args) = parser.parse_args()
    return vars(options)


def main():
    options = parse_args()
    set_of_sites = sites(**options)
    set_of_sites.run()
    set_of_sites.save_to_shp()

if __name__ == "__main__":
    main()

