import utm
import urllib.request, urllib.response
import json
import math
import time
import csv
import shapefile
import os
import matplotlib.pyplot as plt
from operator import attrgetter

#How to increase density of grid inside the central (riverine) area?
denser = 2
#Minimum floodplain_width/channel_width value
ratio = 3

class sites:
    """
    Keeps all the sites with a bunch of Transect instances
    Recieves parameters:
        - amount (number of Transect instances per Site instance)
        - precision (number of Point instances per Transect instances)
        - extent_multiplication (multiplier of the default extent passed from csv source file)
        - infile (/path/to/input.csv)
        - outshp (/path/to/shapefile.shp)
    """
    def list_of_points(self):
        """Read list of sites from file"""
        f = open(self.infile, "r")
        reader = csv.reader(f)
        self.raw = [row for row in reader]
    def run(self):
        """Runs all the main routines in this programm"""
        self.list_of_points()
        self.finished = []
        i = 0
        for item in self.raw:
            i += 1
            print("!!!----------------------------------------------------------------!!!")
            print("!!!----------------------Site {0}/{1} ----------------------------!!!".format(i, self.raw.__len__()))
            new_center_point = point(float(item[1]), float(item[2]))
            new_center_point.from_latlon()
            extent = int(item[3])
            site_id = str(item[0])
            new_rotatoria = rotatoria(new_center_point, self.amount, extent,
                                      self.extent_multiplication, self.precision, site_id)
            self.finished.append(new_rotatoria)
    def validate_shp(self):
        """Working with shapefile caches"""
        #O
        if os.path.isfile(self.outshp):
            print("!!!Old shp file cache detected. Trying to append new data!!!")
            r = shapefile.Reader(self.outshp)
            self.w = shapefile.Writer(r.shapeType)
            # Copy over the existing dbf fields
            self.w.fields = list(r.fields)
            # Copy over the existing dbf records
            self.w.records.extend(r.records())
            # Copy over the existing polygons
            self.w._shapes.extend(r.shapes())
            os.remove(self.outshp)
            print("Old cache shp file deleted")
        else:
            self.w = shapefile.Writer(shapefile.POINT)
            self.w.field("X", "N", 20, 10)
            self.w.field("Y", "N", 20, 10)
            self.w.field("L", "N", 20, 10)
            self.w.field("Z", "N", 20, 10)
            self.w.field("Derivative", "N", 20, 10)
            self.w.field("Site", "C", "20")
            self.w.field("Transect_id", "N", 3, 0)
            self.w.field("Extension","N", 6, 0)
        self.ortoshp = "orto_"+self.outshp
        if os.path.isfile(self.ortoshp):
            print("!!!Old shp file cache detected. Trying to append new data!!!")
            r = shapefile.Reader(self.ortoshp)
            self.orto = shapefile.Writer(r.shapeType)
            # Copy over the existing dbf fields
            self.orto.fields = list(r.fields)
            # Copy over the existing dbf records
            self.orto.records.extend(r.records())
            # Copy over the existing polygons
            self.orto._shapes.extend(r.shapes())
            os.remove(self.ortoshp)
            print("Old cache shp file deleted")
        else:
            self.orto = shapefile.Writer(shapefile.POINT)
            self.orto.field("Z", "N", 20, 10)
            self.orto.field("L", "N", 20, 10)
            self.orto.field("Derivative", "N", 20, 10)
            self.orto.field("Site", "C", "20")
            self.orto.field("Channel_width","N", 6, 0)
    def save_to_shp(self):
        self.validate_shp()
        sites_counter, points_counter = 0, 0
        for item in self.finished:
            #print ("finished len: {0}".format(self.finished.__len__()))
            #ortogonal_transect_point_counter = 0
            for p in item.transects[item.ortogonal_transect].points:
                #ortogonal_transect_point_counter += 1
                    if (p.z != -999) and ((p.derivative != -999) and (p.derivative is not None)):
                        self.orto.point(p.lon, p.lat)
                        self.orto.record(p.z, p.l, p.derivative, item.site_id, item.extent)
            sites_counter += 1
            transects_counter = 0
            for elem in item.transects:
                #print ("transects len: {0}".format(item.transects.__len__()))
                transects_counter += 1
                for p in elem.points:
                    if (p.z != -999) and ((p.derivative != -999) and (p.derivative is not None)):
                        #print(p.derivative)
                        points_counter += 1
                        self.w.point(p.lon, p.lat)
                        self.w.record(p.x, p.y, p.l, p.z, p.derivative, item.site_id, transects_counter, item.extent)
        self.orto.save("orto_"+self.outshp)
        self.w.save(self.outshp)
        print("Saved {0} valid points in {1} site(s))".format(points_counter, sites_counter))
    def __init__(self, **kwargs):
        """Constructor of Sites instances"""
        self.amount = kwargs.get("amount", 4)
        self.precision = kwargs.get("precision", 32)
        self.extent_multiplication = kwargs.get("extent_multiplication", 1)
        self.infile = kwargs.get("infile", "list.csv")
        self.outshp = kwargs.get("outshp", "cache.shp")

class rotatoria:
    """Bunch of transects"""
    def extent_xy_to_latlong(self):
        """Specififes the scale of transection"""
        print("extent_xy_to_latlong(self): center point: "), self.center_point.__str__()
        upper_limit = self.center_point.x + self.diameter_utm/2
        lower_limit = self.center_point.x - self.diameter_utm/2
        print(upper_limit, lower_limit)
        self.up = point(upper_limit, self.center_point.y)
        self.up.from_xy(self.zone)
        self.low = point(lower_limit, self.center_point.y)
        self.low.from_xy(self.zone)
        print("extent_xy_to_latlong(self): up point: "), self.up.__str__()
        print("extent_xy_to_latlong(self): lo point: "), self.low.__str__()
        return self.up.lon - self.low.lon

    def edge_point(self, angle):
        """Returns the new point using the specified direction and radius"""
        #new_lat = self.center_point.lat + self.radius_latlon*math.cos(angle)
        #new_lon = self.center_point.lon + self.radius_latlon*math.sin(angle)
        #new_point = point(new_lat, new_lon)
        #new_point.from_latlon()
        new_x = self.center_point.x + self.diameter_utm/2*math.cos(angle)
        new_y = self.center_point.y + self.diameter_utm/2*math.sin(angle)
        new_point = point(new_x, new_y)
        new_point.from_xy(self.zone)
        return new_point
    def rotate(self):
        """Defines a list of transects"""
        transects = []
        circle = 360
        step = circle/(self.amount*2)
        for i in range(0, self.amount):
            begin_angle = math.radians(i*step)
            begin = self.edge_point(begin_angle)
            end_angle = math.radians(i*step + 180)
            end = self.edge_point(end_angle)
            #print begin.lat, begin.lon
            #print("Rotating: i: {0} step: {1} begin_angle {2} end_angle {3}".format(i, step, begin_angle, end_angle))
            #print("Rotating: begin "), begin.__str__()
            #print("Rotating:   end "), end.__str__()
            new_transect = transect(begin, end, self.precision, self.zone, self.extent)
            transects.append(new_transect)
        return transects
    def choose_ortogonal(self):
        widthes = {}
        for i in range(self.transects.__len__()):
            width = self.transects[i].define_coastline()
            widthes[i]=width
        ortogonal_transect = min(widthes, key = widthes.get)
        return ortogonal_transect
    def __init__(self, point, amount, extent, extent_multiplication, precision, site_id):
        """Transect class constructor"""
        self.center_point = point
        self.zone = point.zone()
        self.amount = amount
        self.extent = extent
        self.extent_multiplication = extent_multiplication
        self.precision = precision
        self.site_id = site_id
        self.diameter_utm = self.extent*self.extent_multiplication
        #print("diameter_utm {0} extent {1} extent_multiplication {2}".format(self.diameter_utm, self.extent, self.extent_multiplication))
        self.radius_latlon = self.extent_xy_to_latlong()/2
        self.transects = self.rotate()
        self.ortogonal_transect = self.choose_ortogonal()

class transect:
    """Transect class for storing the flooadplain transections derived from Google Elevation API"""
    def __init__(self, begin, end, samples, zone, extent):
        """Constructor for transect class"""
        self.samples = samples
        self.extent = extent
        self.begin = begin
        self.end = end
        self.zone = zone
        self.template ="""http://maps.googleapis.com/maps/api/elevation/json?locations={0},{1}&sensor=false"""
        self.points = []
        #self.const_sequence()
        self.specialised_sequence()
        self.grab_GE_api()
        self.numerical_derivative()
    def const_sequence(self):
        """Constructing the point sequence for the current transect"""
        dy = self.end.y - self.begin.y
        dx = self.end.x - self.begin.x
        if dx != 0:
            k = dy/dx
            b = self.end.y - k*self.end.x
            for i in range(self.samples):
                cur_x = self.begin.x + i*dx/self.samples
                cur_y = k*cur_x + b
                cur_point = point(cur_x, cur_y)
                cur_point.from_xy(self.zone)
                dist = self.distance(self.begin, cur_point)
                cur_point.l = dist
                self.points.append(cur_point)
        else:
            cur_x = self.begin.x
            for i in range(self.samples):
                cur_y = self.begin.y + i*dy/self.samples
                cur_point = point(cur_x, cur_y)
                cur_point.from_xy(self.zone)
                dist = self.distance(self.begin, cur_point)
                cur_point.l = dist
                self.points.append(cur_point)
    def specialised_sequence(self):
        """Denser at the riverbed"""
        global denser
        dy = self.end.y - self.begin.y
        dx = self.end.x - self.begin.x
        print("end "), self.end.__str__()
        print("beg "), self.begin.__str__()
        print("dy {0} dx {1}".format(dy, dx))
        diameter = self.distance(self.begin, self.end)
        normal_density = diameter/self.samples
        extra_density = self.extent/denser
        #b = self.end.y - k*self.end.x
        if dx != 0:
            k = dy/dx
            if dy > 0:
                if dx > 0:
                    edge = self.begin
                else:
                    edge = self.end
            else:
                if dx > 0:
                    edge = self.begin
                else:
                    edge = self.end
            cur_point = edge
            cur_point.l = 0
            self.points.append(cur_point)
            while cur_point.l < diameter:
                last_point = self.points[-1]
                if (cur_point.l >= (0.5*diameter - 3*self.extent)) and (cur_point.l <= (0.5*diameter + 3*self.extent)):
                    dl = extra_density
                else:
                    dl = normal_density
                cur_x = last_point.x + dl*math.cos(math.atan(k))
                cur_y = last_point.y + dl*math.sin(math.atan(k))
                cur_point = point(cur_x, cur_y)
                cur_point.from_xy(self.zone)
                dist = self.distance(edge, cur_point)
                cur_point.l = dist
                self.points.append(cur_point)
        else:
            if self.begin.y > self.end.x:
                edge = self.end
            else:
                edge = self.begin
            cur_point = edge
            cur_point.l = 0
            cur_x = cur_point.x
            self.points.append(cur_point)
            while cur_point.l < diameter:
                last_point = self.points[-1]
                if (cur_point.l >= (0.5*diameter - 2*self.extent)) and (cur_point.l <= (0.5*diameter + 2*self.extent)):
                    dl = extra_density
                else:
                    dl = normal_density
                cur_y = last_point.y + dl
                cur_point = point(cur_x, cur_y)
                cur_point.from_xy(self.zone)
                dist = self.distance(edge, cur_point)
                cur_point.l = dist
                self.points.append(cur_point)
    def grab_GE_api(self):
        """Request to Google Elevation API"""
        for item in self.points:
            time.sleep(0.5)
            request = self.template.format(item.lat, item.lon)
            print(request)
            try:
                response = urllib.request.urlopen(request)
                str_response = response.readall().decode('utf-8')
                data = json.loads(str_response)
                print(data)
                if data["status"] == "OK":
                    item.z = data["results"][0]["elevation"]
                else:
                    item.z = -999
                    return
            except:
                print("Failed to request Google Elevation API")
                item.z = -999
                continue
    def numerical_derivative(self):
        """Simple numeric scheme to define derivatives"""
        for i in range(1, self.points.__len__()-1):
            prev = self.points[i-1]
            post = self.points[i+1]
            if (prev.z == -999) or (post.z == -999):
                self.points[i].derivative = -999
            else:
                self.points[i].derivative = abs(round((prev.z - post.z)/(self.distance(prev, post)), 4))
        print("derivatives: {0}".format([t.derivative for t in self.points]))

    def distance(self, p1, p2):
        """Just euclidian distance..."""
        t = math.sqrt((p1.x - p2.x)**2 + (p1.y - p2.y)**2)
        print("x1 {0} x2 {1} y1 {2} y2 {3} dist {4}".format(p1.x, p2.x, p1.y, p2.y, t))
        return t
    def define_coastline(self):
        number_of_points = self.points.__len__()
        if number_of_points % 2 == 0:
            self.transect_center_point_index = int(number_of_points/2)
        else:
            self.transect_center_point_index = int((number_of_points-1)/2)
        print("Transect center_point id {0}".format(self.transect_center_point_index))
        self.transect_center_point = self.points[self.transect_center_point_index]
        center_z = self.transect_center_point.z
        i = j = self.transect_center_point_index
        #walk upstairs
        while (self.points[i].z <= center_z) and (i < self.points.__len__()-1):
            i += 1
            print(i, number_of_points)
        #walk downstairs
        while self.points[j].z <= center_z and (j > 0):
            j -= 1
            print(j, number_of_points)
        channel_width = self.distance(self.points[i], self.points[j])
        print("Channel width among this transect is {0}".format(channel_width))
        return channel_width

class point:
    """Simple point class for meadow-transect utility"""
    def __init__(self, X, Y, z = -999, l = 0, derivative = None):
        """Point class constructor: X, Y - unknow coordinates, Z - """
        self.raw_X = X
        self.raw_Y = Y
        self.z = z
        self.l = l
        self.derivative = derivative
    def __str__(self):
        print("class point object: x: {0}, y: {1}, lat: {2}, lon: {3}, l: {4}, z: {5}".format(self.x, self.y, self.lat, self.lon, self.l, self.z))
    def from_latlon(self):
        """Conversion from geographic coordinates to UTM"""
        self.lat = self.raw_X
        self.lon = self.raw_Y
        result = utm.from_latlon(self.raw_X, self.raw_Y)
        self.x = result[0]
        self.y = result[1]
    def from_xy(self, zone_args):
        """Conversion from UTM to geographic coordinates"""
        self.x = self.raw_X
        self.y = self.raw_Y
        args = (self.x, self.y, zone_args[0], zone_args[1])
        result = utm.to_latlon(*args)
        self.lat = result[0]
        self.lon = result[1]
    def zone(self):
        if self.lat and self.lon:
            result = utm.from_latlon(self.lat, self.lon)
            return (result[2], result[3])

class cross_section(transect):
    def __init__(self, site_id, extent):
        """New cross section initiated"""
        self.site_id = site_id
        self.extent = extent
        self.points = []
    def get_valley_slopes(self):
        number_of_points = self.points.__len__()
        if number_of_points % 2 == 0:
            self.transect_center_point_index = int(number_of_points/2)
        else:
            self.transect_center_point_index = int((number_of_points-1)/2)
        print("Transect cp id {0}/{1}".format(self.transect_center_point_index, number_of_points))
        self.transect_center_point = self.points[self.transect_center_point_index]
        #center_z = self.transect_center_point.z
        self.max_i = self.max_j = self.transect_center_point_index
        self.max_i += (1*denser + int((ratio-1)/2*denser))
        self.max_j -= (1*denser + int((ratio-1)/2*denser))
        self.max_up = self.max_down = 0.0
        i = self.max_i
        j = self.max_j
        #walk upstairs
        while (i < number_of_points-2):
            #print(i, number_of_points,self.points[i].derivative)
            if (self.points[i].derivative >= self.max_up) and ((self.points[i+1].z-self.points[i-1].z)>0):
                self.max_up = self.points[i].derivative
                self.max_i = i
            i += 1
        #walk downstairs
        while (j >= 1):
            if (self.points[j].derivative >= self.max_down) and ((self.points[j-1].z-self.points[j+1].z)>0):
                self.max_down = self.points[j].derivative
                self.max_j = j
            j -= 1
        self.upper_slope = self.points[self.max_i+1]
        self.down_slope = self.points[self.max_j-1]
        self.adjust_valley_slopes()
    def adjust_valley_slopes(self):
        if (self.upper_slope.z != self.down_slope.z):
            slopes = [self.upper_slope, self.down_slope]
            slope_minimum = min(slopes, key=attrgetter('z'))
            slope_maximum = max(slopes, key=attrgetter('z'))
            if slope_maximum == self.down_slope:
                it = self.max_j-1
                while (it < self.transect_center_point_index) and ((self.points[it].z - slope_minimum.z)>0):
                    it += 1
                self.down_slope = self.points[it-1]
            if slope_maximum == self.upper_slope:
                it = self.max_i+1
                while (it > self.transect_center_point_index) and ((self.points[it].z - slope_minimum.z)>0):
                    it -= 1
                self.upper_slope = self.points[it+1]
            self.valley_width = abs(self.upper_slope.l - self.down_slope.l)
    def plot_profile(self):
        l = [p.l for p in self.points]
        z = [p.z for p in self.points]
        xmin = min(l)
        xmax = max(l)
        ymin = min(z)
        ymax = max(z)
        plt.plot(l,z)
        plt.axis([xmin, xmax, ymin, ymax])
        plt.title("Cross-section for site {0}".format(self.site_id))
        plt.xlabel("L, km")
        plt.ylabel("Z, m")
        plt.arrow(self.upper_slope.l, self.upper_slope.z+(ymax-ymin)*0.15,
                0, (ymax-ymin)*(-0.1),
                head_width=(xmax-xmin)*0.03, head_length=(ymax-ymin)*0.05,
                width = (xmax-xmin)*0.01, color="red", alpha=0.5)
        plt.arrow(self.down_slope.l, self.down_slope.z+(ymax-ymin)*0.15,
                0, (ymax-ymin)*(-0.1),
                head_width=(xmax-xmin)*0.03, head_length=(ymax-ymin)*0.05,
                width = (xmax-xmin)*0.01, color="red", alpha=0.5)
        #plt.show()
        plt.savefig(self.site_id+".png")
        plt.clf()

class orto_sites:
    def __init__(self, **kwargs):
        self.infile = kwargs.get("infile","orto_cache.shp")
        self.outfile = kwargs.get("outfile", "valleys.log")
        self.cross_sections = {}
        self.get_shp_data()
        self.save_results()
    def get_shp_data(self):
        self.r = shapefile.Reader(self.infile)
        self.raw_points = self.r.shapes()
        self.raw_records = self.r.records()
        for i in range(0,self.raw_points.__len__()):
            coords = self.raw_points[i].points[0]
            records = self.raw_records[i]
            site_id = records[3]
            if site_id not in self.cross_sections:
                self.cross_sections[site_id] = cross_section(site_id, int(records[4]))
            cur_point = point(float(coords[1]), float(coords[0]),
                              float(records[0]), float(records[1]), float(records[2]))
            cur_point.from_latlon()
            self.cross_sections[site_id].points.append(cur_point)
        #print(self.cross_sections)
    def save_results(self):
        with open(self.outfile, "w") as csv_writer:
            w = csv.writer(csv_writer, delimiter=';')
            w.writerow(["Site_id", "Channel_width", "Valley_width"])
            for k, v in self.cross_sections.items():
                self.cross_sections[k].get_valley_slopes()
                w.writerow([v.site_id, v.extent, v.valley_width, v.extent/v.valley_width])
        for k, v in self.cross_sections.items():
            self.cross_sections[k].plot_profile()
