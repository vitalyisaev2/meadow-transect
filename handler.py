#!/usr/bin/python3.2

from meadow_transect import orto_sites
from optparse import OptionParser

def parse_args():
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option("-i", "--infile", dest="infile", type = "string", help="Path to *.csv file with a list of sites")
    (options, args) = parser.parse_args()
    return vars(options)

def main():
    options = parse_args()
    set_of_sites = orto_sites(**options)
    set_of_sites.get_shp_data()

if __name__ == "__main__":
    main()

